/*jshint node: true*/

angular.module('app', [
  'routes.module',
  'navigation.module',
  'app.controllers',
  'app.services',
  'ngTable',
  'ngSanitize'
]);




//require('jquery');
//require('bootstrap');
//require('angular');
//require('ng-table');
//require('angular-rt-popup');
 
 //declare app level module which depends on components
//require('angular').module('app', [
//  'ngTable',
//  'rt.popup',
//  'app.controllers',
//  'app.services',
//  'app.directives',
//  'app.filters'
//]);

//require('./services.js');
//require('./controllers.js');
//require('./directives.js');
//require('./filters.js');
//
//
//// enable HTML5 mode
//require('angular').module('app').config(function ($locationProvider ) {
//  $locationProvider.html5Mode({"enabled": true, "requireBase": false});
//});
//
//// load student info from context parameter (data)
//require('angular').module('app').run(function ($location, student) {
//  student.getInfo($location.search().data);
//});
