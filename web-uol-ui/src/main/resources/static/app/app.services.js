/*jshint node: true*/
// 
// The 3 valid values for "mode" (below) are "test", "qa", and "prod"
// (case sensitive).

// WARNING: setting this value to "prod" will use the production payment URLs!

var services = angular.module('app.services', []);

services.value('config', {
  "mode": "qa",
  "useFakeData": false,
  "studentServiceEndpoint": {
    "test": "/uol-billing/rest/student/accountInfo",
    "qa": "/uol-billing/rest/student/accountInfo",
    "prod": "/uol-billing/rest/student/accountInfo"
  },
  "billingServiceEndpoint": {
	    "test": "/uol-billing/rest/student/billingInfo",
	    "qa": "/uol-billing/rest/student/billingInfo",
	    "prod": "/uol-billing/rest/student/billingInfo"
   },
   "transactionServiceEndpoint": {
	    "test": "/uol-billing/rest/student/transactionInfo",
	    "qa": "/uol-billing/rest/student/transactionInfo",
	    "prod": "/uol-billing/rest/student/transactionInfo"
  },
  "digestServiceEndpoint": {    
    "test": "/uol-billing/rest/util/digest/",
    "qa": "/uol-billing/rest/util/digest/",
    "prod": "/uol-billing/rest/util/digest/"
  },
  "emailServiceEndpoint": {    
    "test": "/uol-billing/rest/util/email/",
    "qa": "/uol-billing/rest/util/email/",
    "prod": "/uol-billing/rest/util/email/"
  },
  "acceptUrl": {    
	    "test": "http://localhost:8080/uol-billing/email.jsp",
	    "qa": "http://uofl.ohecampus.com.qual/uol-billing/email.jsp",
	    "prod": "http://uofl.ohecampus.com/uol-billing/email.jsp"
	  },
  "paymentProcessor": "ogone",
  "paymentProcessors": {
    "ogone": {
      "test": {
        "url": "https://secure.ogone.com/ncol/test/orderstandard.asp",
        "method": "post",
        "fields": {
          "": ""
        }
      },
      "qa": {
        "url": "https://secure.ogone.com/ncol/test/orderstandard.asp",
        "method": "post",
        "fields": {
          "": ""
        }
      },
      "prod": {
        "url": "https://secure.ogone.com/ncol/prod/orderstandard.asp",
        "method": "post",
        "fields": {
          "": ""
        }
      }
    }
  }
});

services.value('data', {
  "tab": {
    "tabs": [
      {
        "enabled": true,
        "icon": "icon-uniE603",
        "title": "Account Overview",
        "view": "views/account_overview.jsp"
      },
      {
        "enabled": true,
        "icon": "icon-billinghistory",
        "title": "Billing History",
        "view": "views/billing_history.jsp"
      },
      {
        "enabled": true,
        "icon": "icon-credithistory",
        "title": "Payment History",
        "view": "views/payment_history.jsp",
        "disabled": false	
      },
      {
          "enabled": true,
          "icon": "icon_transactionhistory",
          "title": "Transaction History",
          "view": "views/transaction_history.jsp",
          "disabled": false
      },
      {
        "enabled": true,
        "icon": "icon-uniE604",
        "title": "Payment Centre",
        "view": "views/payment_centre.jsp",
        "disabled": false
      }
    ],
    "tabIndex": 0
  }
});

//Account Summary Data
services.factory('student', function ($http, data, config, fakeStudent) {
	  var student = {};
	  token="";
	  student.getInfo = function () {
	    if (config.useFakeData) {
	      console.warn('using fake data instead of real data');
	      data.error = null;
	      student.info = fakeStudent.info;
	      student.account = fakeStudent.account;
	      return;
	    }
	    //if (!token) {
	    //  data.error = {};
	    //  data.error.message = 'We were unable to verify your identity. Please access this page through the University of Liverpool LENS web site.';
	    //  data.error.httpStatus = 'missing token';
	    //  return;
	    //}
	    data.wait = true;
	    var restURL = config.studentServiceEndpoint[config.mode];
	    //var restURL = config.studentServiceEndpoint[config.mode] + ';systemId=32;studentId=helen.culligan@my.ohecampus.com' ;
	    var response = $http.get(restURL);
	    response.success(function (httpData) {
	      data.wait = false;
	      var errorCount = httpData.errors.length;
	      if (errorCount > 0) {
	        data.error = {};
	        data.error.message = 'We were unable to verify your identity. Please try opening the application again.';
	        data.error.httpStatus = httpData.errors.join('; ');
	        return;
	      }
	      data.error = null;
	      student.info = httpData.info;
	      student.account = httpData.account;
	    });
	    response.error(function (httpData, httpStatus, httpHeaders, httpConfig) {
	      data.wait = false;
	      data.error = {};
	      data.error.message = 'Could not validate user. Please try again later.\n\n';
	      data.error.httpData = httpData;
	      data.error.httpStatus = httpStatus;
	      data.error.httpHeaders = httpHeaders;
	      data.error.httpConfig = httpConfig;
	    });
	  };
	  student.update = function () {
	    student.getInfo(student.token);
	  };
	  return student;
	});

//BillingHistory and Invoice Data
services.factory('billingHistory', function ($http, data, config, fakeStudent) {
	  var billingHistory = {};
	  
	  billingHistory.getInfo = function () {
	    data.wait = true;
	    var restURL = config.billingServiceEndpoint[config.mode];
	    var response = $http.get(restURL);
	    response.success(function (httpData) {
	      data.wait = false;
	      var errorCount = httpData.errors.length;
	      if (errorCount > 0) {
	        data.error = {};
	        data.error.message = 'We were unable to verify your identity. Please try opening the application again.';
	        data.error.httpStatus = httpData.errors.join('; ');
	        return;
	      }
	      
    	  billingHistory.billingHistory = httpData.billingHistory;
    	  billingHistory.paymentHistory = httpData.paymentHistory;
	    });
	    response.error(function (httpData, httpStatus, httpHeaders, httpConfig) {
	      data.wait = false;
	      data.error = {};
	      data.error.message = 'Could not validate user. Please try again later.\n\n';
	      data.error.httpData = httpData;
	      data.error.httpStatus = httpStatus;
	      data.error.httpHeaders = httpHeaders;
	      data.error.httpConfig = httpConfig;
	    });		
	  };
	  return billingHistory;
	});

//paymentHistory and transaction history
services.factory('transactionHistory', function ($http, data, config, fakeStudent) {
	  var transactionHistory = {};
	  
	  transactionHistory.getInfo = function () {
	    data.wait = true;
		var restURL = config.transactionServiceEndpoint[config.mode];
	    var response = $http.get(restURL);
	    response.success(function (httpData) {
	      data.wait = false;
	      var errorCount = httpData.errors.length;
	      if (errorCount > 0) {
	        data.error = {};
	        data.error.message = 'We were unable to verify your identity. Please try opening the application again.';
	        data.error.httpStatus = httpData.errors.join('; ');
	        return;
	      }
	      
	      transactionHistory.transactionHistory = httpData.transactionHistory;
	    });
	    response.error(function (httpData, httpStatus, httpHeaders, httpConfig) {
	      data.wait = false;
	      data.error = {};
	      data.error.message = 'Could not validate user. Please try again later.\n\n';
	      data.error.httpData = httpData;
	      data.error.httpStatus = httpStatus;
	      data.error.httpHeaders = httpHeaders;
	      data.error.httpConfig = httpConfig;
	    });		
	  };
	  return transactionHistory;
	});

//services.factory('accountInfo', function ($http, data, config, fakeStudent) {
//	var accountInfo = {};
//	bannerId = "H00012345";
//	
//	accountInfo.getInfo = function() {
//		var invoiceInfo = "";
//		var studentInfo = "";
//		var restURL = config.studentServiceEndpoint[config.mode]
//				+ ';bannerId=' + bannerId;
//		var response = $http.get(restURL);
//		response.success(function(httpData) {
//			data.error = null;
//			$.each(httpData, function(index, value) {
//				if (index == "InvoiceList") {
//					var jsonObj = [];
//
//					accountInfo.invoiceInfo = value;
//				} else if (index == "studentInfo") {
//					accountInfo.studentInfo = value;
//					accountInfo.studentInfo = JSON.parse(accountInfo.studentInfo);
//				}
//			});
//	
//			accountInfo = httpData;
//		});
//		response
//				.error(function(httpData, httpStatus,
//						httpHeaders, httpConfig) {
//					data.wait = false;
//					data.error = {};
//					data.error.message = 'Could not validate user. Please try again later.\n\n';
//						data.error.httpData = httpData;
//						data.error.httpStatus = httpStatus;
//						data.error.httpHeaders = httpHeaders;
//						data.error.httpConfig = httpConfig;
//					});
//		};
//	
//		accountInfo.update = function() {
//			accountInfo.getInfo(accountInfo.bannerId);
//		};
//		return accountInfo;
//	});


services.value('fakeStudent', {
  "info": {
    "userId": "12345",
    "bannerId": "X12345678"
  },
  "account": {
    "paymentPlan": "Module/Pay As You Go",
    "currencyType": "USD",
    "currencySymbol": "$",
    "amount": "123.4",
    "balanceDue": "1234.5",
    "totalBilled": "6543.2",
    "totalPayments": "5555.5",
    "totalObligation": "6666.6",
    "usedStudyPoints": "123",
    "invoicedStudyPoints": "321",
    "holds": "0"
  },
  "billingHistory": [
    {
      "invoiceNumber": "5",
      "paymentPlan": "Module/Pay As You Go",
      "currencyType": "USD",
      "currencySymbol": "$",
      "description": "Tuition and Fees",
      "amount": "1123.5",
      "balanceDue": "11234.5",
      "totalBilled": "16543.2",
      "invoiceDate": "1 Jan 2011",
      "dueDate": "28 Feb 2011",
      "daysLate": "100",
      "status": "Open",

      "personType": "X12345678",
      "firstName": "Fred",
      "lastName": "Flintstone",
      "institutionName": "University of Rocksford",

      "addressType": "MA",
      "address1": "301 Cobblestone Way",
      "address2": "",
      "address3": "",
      "city": "Bedrock",
      "stateCode": "LA",
      "state": "Louisiana",
      "postalCode": "70777",
      "countryCode": "US",
      "country": "Unites States",

      "emailId": "Y",
      "emailAddressType": "A",
      "emailAddressCode": "UNIV",
      "emailAddress": "freddie.f@veristone.net",

      "phoneType": "?",
      "phoneCountryCode": "+1",
      "phoneAreaCode": "432",
      "phoneNumber": "555-4318",
      "phoneExtension": "2112"
    },
    {
      "invoiceNumber": "2",
      "paymentPlan": "Module/Pay As You Go",
      "currencyType": "USD",
      "currencySymbol": "$",
      "description": "Tuition and Fees",
      "amount": "2123.4",
      "balanceDue": "21234.5",
      "totalBilled": "26543.2",
      "invoiceDate": "1 Jan 2012",
      "dueDate": "28 Feb 2012",
      "daysLate": "0",
      "status": "Open",

      "personType": "X12345678",
      "firstName": "Fred",
      "lastName": "Flintstone",
      "institutionName": "University of Rocksford",

      "addressType": "MA",
      "address1": "301 Cobblestone Way",
      "address2": "",
      "address3": "",
      "city": "Bedrock",
      "stateCode": "LA",
      "state": "Louisiana",
      "postalCode": "70777",
      "countryCode": "US",
      "country": "Unites States",

      "emailId": "Y",
      "emailAddressType": "A",
      "emailAddressCode": "UNIV",
      "emailAddress": "freddie.f@veristone.net",

      "phoneType": "?",
      "phoneCountryCode": "+1",
      "phoneAreaCode": "432",
      "phoneNumber": "555-4318",
      "phoneExtension": "2112"
    },
    {
      "invoiceNumber": "3",
      "paymentPlan": "Module/Pay As You Go",
      "currencyType": "USD",
      "currencySymbol": "$",
      "description": "Tuition and Fees",
      "amount": "3123.4",
      "balanceDue": "31234.5",
      "totalBilled": "36543.2",
      "invoiceDate": "1 Jan 2013",
      "dueDate": "28 Feb 2013",
      "daysLate": "0",
      "status": "Open",

      "personType": "X12345678",
      "firstName": "Fred",
      "lastName": "Flintstone",
      "institutionName": "University of Rocksford",

      "addressType": "MA",
      "address1": "301 Cobblestone Way",
      "address2": "",
      "address3": "",
      "city": "Bedrock",
      "stateCode": "LA",
      "state": "Louisiana",
      "postalCode": "70777",
      "countryCode": "US",
      "country": "Unites States",

      "emailId": "Y",
      "emailAddressType": "A",
      "emailAddressCode": "UNIV",
      "emailAddress": "freddie.f@veristone.net",

      "phoneType": "?",
      "phoneCountryCode": "+1",
      "phoneAreaCode": "432",
      "phoneNumber": "555-4318",
      "phoneExtension": "2112"
    }
  ],
  "paymentHistory": [
    {
      "payment_id": "1",
      "payment_date": "12-Apr-2014",
      "transaction_method": "Credit",
      "amount": "-500.0"
    },
    {
      "payment_id": "2",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "3",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "4",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "5",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "6",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "7",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "8",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "9",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "10",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "11",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "12",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "13",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.00"
    },
    {
      "payment_id": "14",
      "payment_date": "12-May-2014",
      "transaction_method": "Visa Card",
      "amount": "US $-3500.0"
    },
    {
      "payment_id": "15",
      "payment_date": "12-June-2014",
      "transaction_method": "PayPal",
      "amount": "-3500.0"
    }
  ]
});