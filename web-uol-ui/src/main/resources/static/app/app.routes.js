angular.module('routes.module', [ 'ngRoute' ])
  .config(function($routeProvider, $httpProvider) {

	$routeProvider.when('/', {
		templateUrl : '/app/components/navigation/navigationView.html',
		controller : 'navigation.controller'
	}).when('/login', {
		templateUrl : '/app/components/login/loginView.html',
		controller : 'login.controller'
	}).otherwise('/');

    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

});