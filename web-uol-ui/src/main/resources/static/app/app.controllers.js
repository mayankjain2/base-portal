var controllers = angular.module('app.controllers', []);
controllers.controller('ErrorController', function ($scope, data, config) {
  $scope.error = data.error;
  $scope.config = config;
});

controllers.controller('TabController', function ($scope, data, student, billingHistory, transactionHistory , config, $http) {
	
	  $scope.symbol = mapSymbol;
	  
	  $scope.paymentButton = "";
	  
	  $scope.setActiveTab = function (index) {
		  if($scope.getTab(index).disabled)
		        return;
	    data.tab.tabIndex = index;
	  };
	  $scope.getActiveTab = function () {
	    return data.tab.tabs[data.tab.tabIndex];
	  };
	  $scope.isActiveTab = function (index) {
	    return index === data.tab.tabIndex;
	  };
	  
	  $scope.isEnabledTab = function (index) {
	    return $scope.getTab(index).enabled;
	  };
	  
	  $scope.getTabs = function () {
	    return data.tab.tabs;
	  };
	  $scope.getTab = function (index) {
	    return data.tab.tabs[index];
	  };
	  $scope.getActiveView = function () {
	    if (data.wait) {
	      return 'views/wait.jsp';
	    }
	    if (data.error && !config.useFakeDataOnError) {
	      return 'views/error.jsp';
	    }
	    return $scope.getActiveTab().view;
	  };
	  
	  $scope.goToPaymentTab = function () {
	    data.tab.tabIndex = 4;
	  };
	  
	  if($scope.getActiveTab().title == 'Account Overview'){
		  if($scope.student == null || $scope.student == undefined)
		  $scope.student = student.getInfo(); 
	  }
	  
	  if(($scope.getActiveTab().title == 'Billing History')){
		  
		   if ($scope.billingHistory == null || $scope.billingHistory == undefined){
			   $scope.billingHistory = billingHistory.getInfo(); 
			}
	  }
	  
	  if($scope.getActiveTab().title != 'Payment Centre'){
		  $scope.paymentButton = true;
	  }else{
		  $scope.paymentButton = false;
	  }
	  
	  if(($scope.getActiveTab().title == 'Transaction History') || ($scope.getActiveTab().title == 'Payment History')){
		  
		   if ($scope.transactionHistory == null || $scope.transactionHistory == undefined){
			   $scope.transactionHistory = transactionHistory.getInfo(); 
			}
	 }
	  $scope.holds = false;
	});


controllers.controller("AccountOverviewController", function ($scope, $location, student, billingHistory, $filter) {
	$scope.student = student;
	
	if($scope.student.account.holds != undefined){
		$scope.holds = true;
	}else{
		$scope.holds = false;
	}
	
	 $scope.today = new Date();
	 
});

controllers.controller("BillingHistoryController", function ($scope, $location, $filter, student, billingHistory, ngTableParams, $http ) {
	
	console.log('billingHistory.billingHistory :', billingHistory.billingHistory);
	$scope.billingHistory = billingHistory.billingHistory;
	$scope.billingData = false;
	if($scope.billingHistory != null || $scope.billingHistory != undefined){
		
	angular.forEach($scope.billingHistory,function(el,i){
		  el.invoiceNumber = parseInt(el.invoiceNumber);
		  el.amount = parseFloat(el.amount);
		  //el.dueDate = Date.parse($filter('date')( moment(el.dueDate), "YYYY-MM-dd"));
		  //el.invoiceDate = Date.parse($filter('date')(moment(el.invoiceDate),"YYYY-MM-dd"));
		});
  	  var data = $scope.billingHistory;
  	  if(data.length >0 ){
  	  	$scope.billingData = true;  
  	  }
	  $scope.student = student;
	    $scope.tableParams = new ngTableParams(
	      {
	        page: 1,
	        count: 10,
	        sorting: {
	          invoiceDate: 'desc',
	          invoiceNumber: 'desc'// initial sorting
	        }
	      },
	      {
	        counts: [],
	        total: 1,
	        getData: function ($defer, params) {
	          var orderedData = params.sorting ? $filter('orderBy')(data, params.orderBy()) : data;
	          $scope.page = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
	          params.total(orderedData.length); // set total for recalc pagination
	          $defer.resolve($scope.page);
	        }
	      });
	}

//    data=_.map($scope.billingHistory,function(row){
//    row=_.clone(row);
//    // Exepected format "yyyy-MMM-dd
//    var invDate = moment(row.invoiceDate);
//    row.invoiceDate = $filter('date')(invDate, "YYYY-MM-dd");
//    
//    var dueDate = moment(row.dueDate);
//    row.dueDate = $filter('date')(dueDate, "YYYY-MM-dd");
//    
//    
//    return row;
//  });
  
      
  $scope.invoiceSelected = function (scope) {
    var invoiceNumber = scope;
    var billingList = $scope.billingHistory;
    var invoiceIndex = 0 ;
    for( var i = 0; i<billingList.length; i++){
        if(billingList[i].invoiceNumber == invoiceNumber)
            invoiceIndex = i;
    }
    var newWindow = window.open('views/invoice/invoice.html');
    newWindow.billingHistory = $scope.billingHistory;
    console.log("invoiceIndex: ", invoiceIndex);
    newWindow.billingHistory.invoiceIndex = invoiceIndex;
    console.log("newWindow.billingHistory.invoiceIndex: ",newWindow.billingHistory.invoiceIndex);
  };
});

controllers.controller("EmailController", function ($scope, $location, $filter, config, student) {
    //Email functionality
       var query = location.search.substr(1);
       var result = {};
       query.split("&").forEach(function(part) {
         var item = part.split("=");
         result[item[0]] = decodeURIComponent(item[1]);
       });
       var item = result;
       //console.log("item:", item);
       $scope.orderID = item.orderID;
       $scope.currency = item.currency;
       $scope.paidAmount = item.amount + " " + item.currency;
       $scope.Beneficiary = 'Laureate Online';
       $scope.PAYID = item.PAYID;
       $scope.studentEmail = item.studentEmail;
       $scope.studentName = item.studentName.replace("+" ," ");
       $scope.studentID = item.studentID;
       var jsonObj = [];
       var newItem = {};
       newItem = {"ORDERID":$scope.orderID,"AMOUNT":$scope.paidAmount,
                  "Beneficiary":$scope.Beneficiary,"PAYID":$scope.PAYID,"BANNER_ID":$scope.studentID,
                  "STUDENT_EMAIL":$scope.studentEmail,"STUDENT_NAME":$scope.studentName};
       jsonObj.push(newItem);
      
       //console.log("json object : ",jsonObj);
       var jsonString =JSON.stringify(jsonObj).substr(1,JSON.stringify(jsonObj).length-2);
       //console.log("jsonString:",jsonString);
       var restURL = config.emailServiceEndpoint[config.mode];
       $.ajax({
           type:"GET",
           crossDomain: true,
           url: restURL + jsonString,
           dataType:"",
           contentType: "application/json; charset=utf-8",
           async: false})
               .success(function(data) {
               });

});

controllers.controller("PaymentHistoryController", function ($scope, $location, student, transactionHistory, billingHistory, $filter, ngTableParams, student ) {
  $scope.transactionHistory = transactionHistory.transactionHistory;
  $scope.billingHistory = billingHistory;
  $scope.paymentData = false;
  $scope.paymentDataCount = 0;
  $scope.currencyType = student.account.currencyType;
  if($scope.transactionHistory != null || $scope.transactionHistory != undefined){
	  
	  var rows = new Array();
	  var j = 0;
	  for(var i = $scope.transactionHistory.length - 1; i >= 0; i--){
		    if($scope.transactionHistory[i].paymentType == 'P'){
		    	$scope.paymentDataCount = $scope.paymentDataCount + 1;
		    	rows[j] = $scope.transactionHistory[i];
		    	j = j +1;
		    }
		}
	  
	angular.forEach(rows,function(el,i){
		  el.invoiceNumber = parseInt(el.invoiceNumber);
		  el.payment = parseFloat(el.payment);
		  //el.transDate = new Date("YYYY-MM-dd");
		  //el.transDate = el.transDate.toString();
		  //el.transDate = Date.parse($filter('date')( moment(el.transDate).toDate(), "YYYY-MM-dd"));
		});
		
	  $scope.testData = rows;
	  var data = rows;
	  if($scope.paymentDataCount>0){
		  $scope.paymentData = true;
	  }
	  
	  $scope.student = student;
	  $scope.tableParams = new ngTableParams(
		      {
		        page: 1,
		        count: 10,
		        sorting: {
		          transDate: 'desc',
		          invoiceNumber: 'desc'// initial sorting
		        }
		      },
		      {
		        counts: [],
		        total: 1,
		        getData: function ($defer, params) {
		          var orderedData = params.sorting ? $filter('orderBy')(data, params.orderBy()) : data;
		          $scope.page = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
		          params.total(orderedData.length); // set total for recalc pagination
		          $defer.resolve($scope.page);
		        }
		      });	  
  }
  
//  data=_.map(rows,function(row){
//	    row=_.clone(row);
//	    // Exepected format "yyyy-MMM-dd
//	    var transDate = moment(row.transDate);
//	    row.transDate = $filter('date')(transDate, "YYYY-MM-dd");
//	    
//	    return row;
//	  });
  
  $scope.invoiceSelected = function (scope) {
	    var invoiceNumber = scope;
	    var billingList = $scope.transactionHistory;
	    var invoiceIndex = 0 ;
	    for( var i = 0; i<billingList.length; i++){
	        if(billingList[i].invoiceNumber == invoiceNumber)
	            invoiceIndex = i;
	    }
	    var newWindow = window.open('views/invoice/invoice.html');
	    newWindow.billingHistory = $scope.transactionHistory;
	    newWindow.billingHistory.invoiceIndex = invoiceIndex;
	  };

});

controllers.controller("TransactionHistoryController", function ($scope, $location, student, transactionHistory, billingHistory, $filter, ngTableParams, student ) {
	  $scope.transactionHistory = transactionHistory.transactionHistory;
	  $scope.totalCharges = 0.00;
	  $scope.totalPaymentsCredits = 0.00;
	  var temp = {};
	  var sumTotalCharges = 0.00;
	  var sumTotalPaymentsCredits = 0.00;
	  $scope.transactionData = false;
	  $scope.transactionDataCount = 0;
	  
	  if($scope.transactionHistory != null || $scope.transactionHistory != undefined){
		  temp  = $scope.transactionHistory;
		  $.each( temp, function( index, value ){
			  sumTotalCharges = parseFloat(sumTotalCharges) + parseFloat(temp[index].amount);
			});
			 
		 $scope.totalCharges = sumTotalCharges;
		 
		 $.each( temp, function( index, value ){
			 sumTotalPaymentsCredits = parseFloat(sumTotalPaymentsCredits) + parseFloat(temp[index].payment);
			});
		
		 $scope.totalPaymentsCredits = sumTotalPaymentsCredits;
		
		 for(var i = $scope.transactionHistory.length - 1; i >= 0; i--){
			    	$scope.transactionDataCount = $scope.transactionDataCount + 1;
			}
		  
		  if($scope.transactionDataCount>0){
			  $scope.transactionData = true;
		  }
		  
			angular.forEach($scope.transactionHistory,function(el,i){
				  el.invoiceNumber = parseInt(el.invoiceNumber);
				  el.amount = parseFloat(el.amount);
				  el.payment = parseFloat(el.payment);
				  //el.transDate = Date.parse($filter('date')( moment(el.transDate), "YYYY-MM-dd"));
				});
		  
		  var data = $scope.transactionHistory;
		  $scope.student = student;
		  $scope.tableParams = new ngTableParams(
			      {
			        page: 1,
			        count: 10,
			        sorting: {
			        	transDate: 'desc',
			        	invoiceNumber: 'desc'// initial sorting
			        }
			      },
			      {
			        counts: [],
			        total: 1,
			        getData: function ($defer, params) {
			          var orderedData = params.sorting ? $filter('orderBy')(data, params.orderBy()) : data;
			          $scope.page = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
			          params.total(orderedData.length); // set total for recalc pagination
			          $defer.resolve($scope.page);
			        }
			      });	  
	  }
	  
//	  data=_.map($scope.transactionHistory,function(row){
//		    row=_.clone(row);
//		    // Exepected format "yyyy-MMM-dd
//		    var transDate = moment(row.transDate);
//		    row.transDate = $filter('date')(transDate, "YYYY-MM-dd");
//		    
//		    return row;
//		  });
	  
	  $scope.invoiceSelected = function (scope) {
		    var invoiceNumber = scope;
		    var billingList = $scope.transactionHistory;
		    console.log("billinglist ", billingList);
		    var invoiceIndex = 0 ;
		    for( var i = 0; i<billingList.length; i++){
		        if(billingList[i].invoiceNumber == invoiceNumber)
		            invoiceIndex = i;
		    }
		    var newWindow = window.open('views/invoice/invoice.html');
		    newWindow.billingHistory = $scope.transactionHistory;
		    newWindow.billingHistory.invoiceIndex = invoiceIndex;
		  };

	});

controllers.controller("PaymentCentreController", function ($scope, student, config, $sce) {
    $scope.acceptURL = "http://"+window.location.host+"/uol-billing/email.jsp";//config.acceptUrl[config.mode];//  "http://localhost:8080/uol-billing/email.jsp";
    $scope.student = student;
    $scope.config = config;
    $scope.paymentType = "CreditCard";
    $scope.totalBalanceDue = student.account.balanceDue.replace("," ,"");;
    $scope.amountValue = student.account.balanceDue.split(".");
    $scope.payment_input_amount = $scope.amountValue[0];
    $scope.payment_input_decimal = $scope.amountValue[1];
    console.log(" $scope.payment_input_amount:",  $scope.payment_input_amount);
    console.log(" $scope.payment_input_decimal:",  $scope.payment_input_decimal);
    if($scope.payment_input_decimal != undefined && $scope.payment_input_decimal.length == 1){
    	 $scope.payment_input_decimal = $scope.payment_input_decimal + "0";	
    }else if($scope.payment_input_decimal != undefined && $scope.payment_input_decimal.length < 1){
    	$scope.payment_input_decimal = $scope.payment_input_decimal + "00";	
    }else if($scope.payment_input_decimal == undefined){
    	$scope.payment_input_decimal = "00";	
    }
    $scope.payment_amount = 0.0;
    $scope.processing_fee =0.0;
    $scope.total_amount = 0.0;
    $scope.final_amount = "";
    $scope.SHASIGN = '';
    $scope.paymenturl ='';

    $scope.getFee = function () {
        // TODO: this is business logic--must be moved out of UI!
        if ($scope.paymentType === "CreditCard" && $scope.student.info.systemId === "32") {
          return 0.025;
        }
        if ($scope.paymentType === "CreditCard" && $scope.student.info.systemId === "27") {
            return 0.0;
          }
        if ($scope.paymentType === "PayPal") {
          return 0.025;
        }
        if ($scope.paymentType === "PeerTransfer") {
          return 0.0;
        }
      };
      

    $scope.paymentTypeChanged = function () {
      $scope.amountChanged();
    };

    $scope.getPaymentType = function () {
      if ($scope.paymentType === "PeerTransfer") {
        return 0;
      }
    };

     $scope.formatAmount = function () {
       $scope.paymentStr = $scope.payment_input_amount.replace("," ,"");
       $scope.decimalStr = $scope.payment_input_decimal;
       $scope.payment_amount = parseFloat($scope.paymentStr+'.'+$scope.decimalStr);
    };
    $scope.amountChanged = function () {
      $scope.formatAmount();
      $scope.payment_amount = parseFloat($scope.payment_amount);
      console.log(" $scope.getFee():",  $scope.getFee());
      $scope.processing_fee = $scope.payment_amount * parseFloat($scope.getFee());
      $scope.processing_fee = $scope.processing_fee.toFixed(2);
      $scope.total_amount = $scope.payment_amount + parseFloat($scope.processing_fee);
      $scope.total_amount = Number($scope.total_amount.toString().match(/^\d+(?:\.\d{0,3})?/));
      $scope.total_amount = $scope.total_amount.toFixed(2);
    };
   
   
  //Send the Fees total payment for payment portal
    $scope.SubmitPayment = function () {
    	
    	$scope.PSPID = null;
        $scope.submitAmount = $scope.total_amount;
        //    Sample SHASIGN 
        //    SHASIGN = ORDERIDAMOUNTCURRENCYPSPID
        //    SHASIGN = 1234-233333301500EURMyPSPIDMysecretsig1875!?
        $scope.ORDERID = $scope.student.info.bannerId + "-" + new Date().getTime();
        $scope.final_amount = $scope.submitAmount.toString().replace(".", "");
        document.paymentForm.action = "";
        $scope.actionUrl = "";
        if ($scope.paymentType != "PeerTransfer") {
          var jsonObj = [];
              var item = {};
              item ["ORDERID"] = $scope.ORDERID;
              item ["AMOUNT"] = $scope.final_amount;
              item ["CURRENCY"] = $scope.student.account.currencyType;
              if($scope.student.info.systemId === "32"){
            	  item ["PSPID"] = 'LaureateOnline';
            	  $scope.PSPID = 'LaureateOnline';
              }
              
              if($scope.student.info.systemId === "27"){
            	  item ["PSPID"] = 'Laureate2';
            	  $scope.PSPID = 'Laureate2';
              }
              
            jsonObj.push(item);

              var jsonString =JSON.stringify(jsonObj).substr(1,JSON.stringify(jsonObj).length-2);
              var restURL = config.digestServiceEndpoint[config.mode];
              $.ajax({
                  type:"GET",
                  url: restURL + jsonString,
                  dataType:"text",
                  contentType: "application/json; charset=utf-8",
                  data:"",
                  async: false})
                      .success(function(data) {
                        $scope.SHASIGN =data;
                      });
            document.paymentForm.action=config.paymentProcessors.ogone[config.mode].url;
            console.log("document.paymentForm.action",document.paymentForm.action);
            document.paymentForm.submit();
          }else{
                var url = 'http://www.peertransfer.com/liverpool';
                document.peerNetworkForm.action=url;
                $("#myPost").attr("method", "get");
          }    	

    };
    $scope.amountChanged();
});


controllers.filter('moment', function () {
	  return function (input, momentFn /*, param1, param2, ...param n */) {
	    var args = Array.prototype.slice.call(arguments, 2),
	        momentObj = moment(input);
	    return momentObj[momentFn].apply(momentObj, args);
	  };
	});

controllers.controller("PopupController", function ($scope, student) {
  $scope.student = student;
  //$scope.accountInfo = accountInfo;
  $scope.show = false;
  $scope.showPopup = function () {
    $scope.show = true;
  };
  $scope.closePopup = function () {
    $scope.show = false;
  };
});
