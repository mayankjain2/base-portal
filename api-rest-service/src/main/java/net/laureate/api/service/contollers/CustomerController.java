/**
 * 
 * 
 */
package net.laureate.api.service.contollers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;



/**
 * 
 * @author snair
 *
 */
@RestController
@ControllerAdvice
@RequestMapping(value="/rest/customer")
//@Api(value = "customer")
public class CustomerController {

	
	@RequestMapping(value="/{customerid}/v1", method = RequestMethod.GET, produces = "application/json")
	//@ApiOperation(value = "get a single customer record", notes = "get details of a customer based on customer id", response = String.class)
	//send http response code 200 for success
	@ResponseStatus(HttpStatus.OK)  
	public String getCustomer(@PathVariable String customerid) {
		return "{test:'test'}";
	}
	
}
