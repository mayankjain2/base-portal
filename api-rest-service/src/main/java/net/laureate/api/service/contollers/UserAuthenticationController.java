/**
 * 
 * 
 */
package net.laureate.api.service.contollers;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;


/**
 * 
 * @author snair
 *
 */
@RestController
@ControllerAdvice
//@Api(value = "login")
public class UserAuthenticationController {

	
	@RequestMapping(value="/user", method = RequestMethod.POST, produces = "application/json")
	public Principal user(Principal user) {
	    return user;
	}
	
    @RequestMapping("/resource")
    public Map<String,Object> home() {
      Map<String,Object> model = new HashMap<String,Object>();
      model.put("id", UUID.randomUUID().toString());
      model.put("content", "Hello World");
      return model;
    }

	
}
