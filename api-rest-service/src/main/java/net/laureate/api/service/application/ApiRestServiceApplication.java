package net.laureate.api.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import net.laureate.api.service.security.CustomUserDetailsService;


@SpringBootApplication
public class ApiRestServiceApplication {

	public static void main(String[] args) {
        SpringApplication.run(ApiRestServiceApplication.class, args);
    }
    
	 @Configuration
	 @ImportResource({"classpath:spring-security.xml"})
	 @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
	 protected static class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	   //[DEL START] Commented by Mayank - to implement oauth
	    /*@Override
	    protected void configure(HttpSecurity http) throws Exception {
	      http
	        .authorizeRequests()
	          .anyRequest().permitAll();
	    }*/
	   //[DEL END]  
	   
	    //[ADD START] Added by Mayank - to implement oauth
		/* @Autowired
			private CustomUserDetailsService userDetailsService;
			
			@Override
			protected void configure(AuthenticationManagerBuilder auth) throws Exception {
				 auth.userDetailsService(userDetailsService);
			}

			@Override
			@Bean
			public AuthenticationManager authenticationManagerBean() throws Exception {
				return super.authenticationManagerBean();
			}*/
	    //[ADD END]
	 }
    
}
