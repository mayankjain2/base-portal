package net.laureate.api.service.application;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//[ADD START] Added by Mayank
@Configuration
@EnableJpaRepositories("net.laureate")
@EntityScan(basePackages = "net.laureate")
//[ADD END]
public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ApiRestServiceApplication.class);
	}

}
