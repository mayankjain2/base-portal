/**
 * 
 * 
 */
package net.laureate.api.service.contollers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;



/**
 * 
 * @author snair
 *
 */
@RestController
@ControllerAdvice
//@Api(value = "configuration")
public class ConfigurationDataController {

	
	@RequestMapping(value="/rest/country/{countrycode}/configuration/v1", method = RequestMethod.GET, produces = "application/json")
	//@ApiOperation(value = "get configuration for a country", notes = "get configuration details of a country based on country code", response = String.class)
	//send http response code 200 for success
	@ResponseStatus(HttpStatus.OK)  
	public String getCountryConfiguration(@PathVariable String countrycode) {
		return "{test:'test'}";
	}
	
	
	@RequestMapping(value="/rest/currency/{currencycode}/configuration/v1", method = RequestMethod.GET, produces = "application/json")
	//@ApiOperation(value = "get configuration for a currency", notes = "get configuration details of a currency based on currency code", response = String.class)
	//send http response code 200 for success
	@ResponseStatus(HttpStatus.OK)  
	public String getCurrencyConfiguration(@PathVariable String currency) {
		return "{test:'test'}";
	}
}
