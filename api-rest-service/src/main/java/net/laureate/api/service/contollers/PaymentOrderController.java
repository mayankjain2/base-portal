/**
 * 
 * 
 */
package net.laureate.api.service.contollers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;



/**
 * 
 * @author snair
 *
 */
@RestController
@ControllerAdvice
@RequestMapping(value="/rest/customers/{customerid}/")
//@Api(value = "payments")
public class PaymentOrderController {

	
	@RequestMapping(value="/payments/{paymentid}/v1", method = RequestMethod.GET, produces = "application/json")
	//@ApiOperation(value = "get a payment record", notes = "Returns a single payment record based on paymentid of a customer", response = String.class)
	//send http response code 200 for success
	@ResponseStatus(HttpStatus.OK)  
	public String getPayment(@PathVariable String customerid,@PathVariable String paymentid) {
		return "";
	
	}
	
	@RequestMapping(value="/payments/v1", method = RequestMethod.GET, produces = "application/json")
	//@ApiOperation(value = "get all payment records", notes = "get all payment records of a customer", response = String.class)
	//200 (OK), list of payments. Use pagination, sorting and filtering to navigate big lists.
	@ResponseStatus(HttpStatus.OK)
	public String getPayments(@PathVariable String customerid) {
		return "";
	}
	
	
	@RequestMapping(value="/payments/v1", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	//@ApiOperation(value = "create a payment record", notes = "create a payment record  of a customer", response = String.class)
	//201 (Created), 'Location' header with link to /payments/{paymentid}/v1 containing new ID.
	//404 (Not Found), 409 (Conflict) if resource already exists..
	@ResponseStatus(HttpStatus.CREATED)
	public String createPayment(@PathVariable String customerid) {
		return "";
	}

	@RequestMapping(value="/payments/{paymentid}/v1", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	//@ApiOperation(value = "update a payment record", notes = "update payment record for a customer based on payment id", response = String.class)
	//200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or invalid.
	@ResponseStatus(HttpStatus.OK)
	public String updatePayment(@PathVariable String customerid,@PathVariable String paymentid) {
		return "";
	
		
	}
	
	
	
	
	
	
	
	
}
