/**
 * 
 * 
 */
package net.laureate.api.service.contollers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;



/**
 * 
 * @author snair
 *
 */
@RestController
@ControllerAdvice
@RequestMapping(value="/rest/customers/{customerid}/")
//@Api(value = "invoices")
public class InvoiceController {

	
	@RequestMapping(value="/invoices/{invoiceid}/v1", method = RequestMethod.GET, produces = "application/json")
	//@ApiOperation(value = "get a single invoice" , notes = "get a single invoice based on invoice id for a customer", response = String.class)
	//send http response code 200 for success
	@ResponseStatus(HttpStatus.OK)
	public String getInvoice(@PathVariable String customerid,@PathVariable String invoiceid) {
		return "{invoice:'test'}";
	}
	
	
	@RequestMapping(value="/invoices/v1", method = RequestMethod.GET, produces = "application/json")
	//@ApiOperation(value = "get list of invoices", notes = "get all invoices for a customer", response = String.class)
	//200 (OK), list of invoices. Use pagination, sorting and filtering to navigate big lists.
	@ResponseStatus(HttpStatus.OK)
	public String getInvoices(@PathVariable String customerid) {
		return "{invoice:'test'}";
	}
}
